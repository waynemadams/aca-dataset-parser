module Main where

import AcaTools
import Test.Hspec

main :: IO ()
main = hspec $ do
  describe "safeSplitCsv" $ do
    let line01 = "\"$400,000.00\",field 2, field 3,\"help, me\""
    let splitLine01 = safeSplitCsv line01
    context "when provided with a test line beginning with a quoted field" $ do
      it "will produce four fields" $ do
        length splitLine01 `shouldBe` 4

      it "will produce a first field equal to '$400,000.00'" $ do
        splitLine01 !! 0 `shouldBe` "$400,000.00"

      it "will produce a second field equal to 'field 2'" $ do
        splitLine01 !! 1 `shouldBe` "field 2"

      it "will produce a third field equal to ' field 3'" $ do
        splitLine01 !! 2 `shouldBe` " field 3"

      it "will produce a fourth field equal to 'help, me'" $ do
        splitLine01 !! 3 `shouldBe` "help, me"

    let line02 = "2.9979245,\"hey, 'Tis I\",,arf"
    let splitLine02 = safeSplitCsv line02
    context ("when provided with a test line starting with an " ++
             "unquoted field, containing a quote-delimited field " ++
             "with a comma, and containing an empty field") $ do
      it "will produce four fields" $ do
        length splitLine02 `shouldBe` 4

      it "will produce a first field equal to '2.9979245'" $ do
        splitLine02 !! 0 `shouldBe` "2.9979245"

      it "will produce a second field equal to \"hey, 'Tis I\"" $ do
        splitLine02 !! 1 `shouldBe` "hey, 'Tis I"

      it "will produce an empty third field" $ do
        splitLine02 !! 2 `shouldBe` ""

      it "will produce a fourth field equal to 'arf'" $ do
        splitLine02 !! 3 `shouldBe` "arf"

  describe "parseDollarAmount" $ do
    context "when given the String \"$400,000.39\"" $ do
      it "will return the Double 400000.39" $ do
        parseDollarAmount "$400,000.39" `shouldBe` Right 400000.39
    context "when given the String \"$.39\"" $ do
      it "will return the Double 0.39" $ do
        parseDollarAmount "$.39" `shouldBe` Right 0.39
    context "when given the String \"$400.39\"" $ do
      it "will return the Double 400.39" $ do
        parseDollarAmount "$400.39" `shouldBe` Right 400.39
    context "when given the String \"2,525.30\"" $ do
      it "will return the Double 2525.30" $ do
        parseDollarAmount "2,525.30" `shouldBe` Right 2525.30
    context "when given the String \"$400,32a.39\"" $ do
      it "will return the failure \"illegal character 'a'\"" $ do
        parseDollarAmount "$400,32a.39" `shouldBe` Left (InvalidCurrencyCharacters "a")
    context "when given the String \"$400,000$.39\"" $ do
      it "will return the failure \"dollar sign(s) wrong position\"" $ do
        parseDollarAmount "$400,000$.39" `shouldBe` Left (InvalidCurrencyCharacters "dollar sign(s) wrong position")
    context "when given the String \"$40,329.33.00\"" $ do
      it "will return the failure \"\"" $ do
        parseDollarAmount "$40,329.33.00" `shouldBe` Left (InvalidCurrencyCharacters ">1 decimal point")
