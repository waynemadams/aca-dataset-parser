module Main where

import AcaCore
import AcaTools
import Test.Hspec

main :: IO ()
main = hspec $ do
  let header = "State,County,Metal Level,Issuer Name,Plan ID - Standard Component,Plan Marketing Name,Plan Type,Rating Area,Child Only Offering,Source,Customer Service Phone Number Local,Customer Service Phone Number Toll Free,Customer Service Phone Number TTY,Network URL,Plan Brochure URL,Summary of Benefits URL,Drug Formulary URL,Adult Dental,Child Dental,Premium Scenarios,Premium Child,Premium Adult Individual Age 21,Premium Adult Individual Age 27,Premium Adult Individual Age 30,Premium Adult Individual Age 40,Premium Adult Individual Age 50,Premium Adult Individual Age 60,Premium Couple 21  ,Premium Couple 30,Premium Couple 40,Premium Couple 50,Premium Couple 60,\"Couple+1 child, Age 21\",\"Couple+1 child, Age 30\",\"Couple+1 child, Age 40\",\"Couple+1 child, Age 50\",\"Couple+2 children, Age 21\",\"Couple+2 children, Age 30\",\"Couple+2 children, Age 40\",\"Couple+2 children, Age 50\",\"Couple+3 or more Children, Age 21\",\"Couple+3 or more Children, Age 30\",\"Couple+3 or more Children, Age 40\",\"Couple+3 or more Children, Age 50\",\"Individual+1 child, Age 21\",\"Individual+1 child, Age 30\",\"Individual+1 child, Age 40\",\"Individual+1 child, Age 50\",\"Individual+2 children, Age 21\",\"Individual+2 children, Age 30\",\"Individual+2 children, Age 40\",\"Individual+2 children, Age 50\",\"Individual+3 or more children, Age 21\",\"Individual+3 or more children, Age 30\",\"Individual+3 or more children, Age 40\",\"Individual+3 or more children, Age 50\",Standard Plan Cost Sharing,Medical Deductible - individual - standard,Drug Deductible - individual - standard,Medical Deductible -family - standard,Drug Deductible - family - standard,Medical Maximum Out Of Pocket - individual - standard,Drug Maximum Out of Pocket - individual - standard,Medical Maximum Out of Pocket - family - standard,Drug Maximum Out of Pocket - Family  - standard,Primary Care Physician  - standard,Specialist  - standard,Emergency Room  - standard,Inpatient Facility  - standard,Inpatient Physician - standard,Generic Drugs - standard,Preferred Brand Drugs - standard,Non-preferred Brand Drugs - standard,Specialty Drugs - standard,73 Percent Actuarial Value Silver Plan Cost Sharing,Medical Deductible - individual - 73 percent,Drug Deductible - individual - 73 percent,Medical Deductible - family - 73 percent,Drug Deductible - family - 73 percent,Medical Maximum Out Of Pocket - individual - 73 percent,Drug Maximum Out of Pocket - individual - 73 percent,Medical Maximum Out of Pocket - family - 73 percent,Drug Maximum Out of Pocket - Family - 73 percent,Primary Care Physician - 73 percent,Specialist - 73 percent,Emergency Room - 73 percent,Inpatient Facility - 73 percent,Inpatient Physician - 73 percent,Generic Drugs - 73 percent,Preferred Brand Drugs - 73 percent,Non-preferred Brand Drugs - 73 percent,Specialty Drugs - 73 percent,87 Percent Actuarial Value Silver Plan Cost Sharing,Medical Deductible - individual - 87 percent,Drug Deductible - individual - 87 percent,Medical Deductible - family - 87 percent,Drug Deductible - family - 87 percent,Medical Maximum Out Of Pocket - individual - 87 percent,Drug Maximum Out of Pocket - individual - 87 percent,Medical Maximum Out of Pocket - family - 87 percent,Drug Maximum Out of Pocket - Family - 87 percent,Primary Care Physician - 87 percent,Specialist - 87 percent,Emergency Room - 87 percent,Inpatient Facility - 87 percent,Inpatient Physician - 87 percent,Generic Drugs - 87 percent,Preferred Brand Drugs - 87 percent,Non-preferred Brand Drugs - 87 percent,Specialty Drugs - 87 percent,94 Percent Actuarial Value Silver Plan Cost Sharing,Medical Deductible - individual - 94 percent,Drug Deductible - individual - 94 percent,Medical Deductible - family - 94 percent,Drug Deductible - family - 94 percent,Medical Maximum Out Of Pocket -individual - 94 percent,Drug Maximum Out of Pocket - individual - 94 percent,Medical Maximum Out of Pocket - family - 94 percent,Drug Maximum Out of Pocket - Family  - 94 percent,Primary Care Physician - 94 percent,Specialist - 94 percent,Emergency Room - 94 percent,Inpatient Facility  - 94 percent,Inpatient Physician  - 94 percent,Generic Drugs - 94 percent,Preferred Brand Drugs - 94 percent,Non-preferred Brand Drugs - 94 percent,Specialty Drugs - 94 percent"
  let splitHeader = safeSplitCsv header
  let sampleLine = "AK,ALEUTIANS EAST,Silver,Premera Blue Cross Blue Shield of Alaska,38344AK0570002,\"Blue Cross Blue Shield Plus 2500 HSA High, a Multi-State Plan\",PPO,Rating Area 2,Allows Adult and Child-Only,OPM,1-800-508-4722,1-800-508-4722,1-800-842-5357,https://www.premera.com/wa/visitor/,https://www.premera.com/documents/007528.pdf,https://www.premera.com/documents/028239.pdf,http://client.formularynavigator.com/Search.aspx?siteCode=7918349883,,X,,$211.00,$333.00,$348.00,$377.00,$425.00,$594.00,$902.00,$666.00,$754.00,$850.00,$1188.00,$1804.00,$877.00,$965.00,$1061.00,$1399.00,$1088.00,$1176.00,$1272.00,$1610.00,$1299.00,$1387.00,$1483.00,$1821.00,$544.00,$588.00,$636.00,$805.00,$755.00,$799.00,$847.00,$1016.00,$966.00,$1010.00,$1058.00,$1227.00,,\"$2,500\",Included in Medical,\"$5,000\",Included in Medical,\"$4,500\",Included in Medical,\"$9,000\",Included in Medical,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,,\"$2,000\",Included in Medical,\"$4,000\",Included in Medical,\"$3,500\",Included in Medical,\"$7,000\",Included in Medical,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,,$750,Included in Medical,\"$1,500\",Included in Medical,\"$1,250\",Included in Medical,\"$2,500\",Included in Medical,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,,$250,Included in Medical,$500,Included in Medical,$500,Included in Medical,\"$1,000\",Included in Medical,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible,20% Coinsurance after deductible"
  let splitLine = safeSplitCsv sampleLine
  let record = parseQHPLandscapeIndividualMarketMedical sampleLine

  describe "The sample Individual Market Medical record" $ do
    context "when parsed with safeSplitCSV" $ do
      it "will contain 128 fields" $ do
        length splitLine `shouldBe` 128
      it "will have field 65 equal to '20% Coinsurance after deductible'" $ do
        splitLine !! 65 `shouldBe` "20% Coinsurance after deductible"
    context "when parsed with parseQHPLandscapeIndividualMarketMedical" $ do
      it "will have State 'AK'" $ do
        state <$> record `shouldBe` Right "AK"
      it "will have individual medical deductible for the 94% silver plan equal to 250.0" $ do
        medicalDeductibleIndividual . silver94PercentPlan <$> record
          `shouldBe` Right (DollarCost 250.0)
      it "will have primary care physician cost for the 94% silver plan equal to '20% Coinsurance after deductible'" $ do
        primaryCarePhysician . silver94PercentPlan <$> record
          `shouldBe` Right (CostDescription "20% Coinsurance after deductible")
      it "will have toll-free number 1-800-508-4722" $ do
        tollFreeNumber . customerServicePhoneNumbers <$> record `shouldBe` Right "1-800-508-4722"
      it "will have drug formulary URL 'http://client.formularynavigator.com/Search.aspx?siteCode=7918349883'" $ do
        drugFormularyURL . urls <$> record `shouldBe` Right "http://client.formularynavigator.com/Search.aspx?siteCode=7918349883"

  describe "The QHP Landscape Individual Market Medical header" $ do
    context "when parsed with safeSplitCsv" $ do
      it "will contain 128 fields" $ do
        length splitHeader `shouldBe` 128
      it "will have first field equal to 'State'" $ do
        splitHeader !! 0 `shouldBe` "State"
      it "will have 33rd field equal to 'Couple+1 child, Age 21'" $ do
        splitHeader !! 32 `shouldBe` "Couple+1 child, Age 21"
      it "will have 56th field equal to 'Individual+3 or more children, Age 50'" $ do
        splitHeader !! 55 `shouldBe` "Individual+3 or more children, Age 50"
      it "will have 100th field equal to 'Medical Maximum Out of Pocket - family - 87 percent'" $ do
        splitHeader !! 99 `shouldBe` "Medical Maximum Out of Pocket - family - 87 percent"
      it "will have 128th field equal to 'Specialty Drugs - 94 percent'" $ do
        splitHeader !! 127 `shouldBe` "Specialty Drugs - 94 percent"

  describe "The parseChildPremiumScenario function" $ do
    context "when given the String \"$249.89\"" $ do
      it "will return a Child PremiumScenario with value 249.89" $ do
        (parseChildPremiumScenario "$249.89") `shouldBe` Right (Child 249.89)
    context "when given the String \"$249..89\"" $ do
      it "will return a Left (InvalidCurrencyCharacters \">1 decimal point\")" $ do
        (parseChildPremiumScenario "$249..89") `shouldBe` Left (InvalidCurrencyCharacters ">1 decimal point")
