module AcaCore where

import AcaTools

-- QHP Landscape Individual Market Medical
type State = String
type County = String
type MetalLevel = String
type IssuerName = String
type PlanIDStandardComponent = String
type PlanMarketingName = String
type PlanType = String
type RatingArea = String
type ChildOnlyOffering = String
type Source = String
type LocalNumber = String
type TollFreeNumber = String
type TTYNumber = String
data CustomerServicePhoneNumbers =
  CustomerServicePhoneNumbers {
    localNumber :: LocalNumber
    , tollFreeNumber :: TollFreeNumber
    , ttyNumber :: TTYNumber
  }
type NetworkURL = String
type PlanBrochureURL = String
type SummaryOfBenefitsURL = String
type DrugFormularyURL = String
data URLs =
  URLs {
    networkURL :: NetworkURL
    , planBrochureURL :: PlanBrochureURL
    , summaryOfBenefitsURL :: SummaryOfBenefitsURL
    , drugFormularyURL :: DrugFormularyURL
  }
type AdultDental = String
type ChildDental = String

-- types for Premium scenarios
type Age = Int
type ChildCount = String -- String, to handle "3 or more" case
type Premium = Double
data PremiumScenario =
  Child {
    child :: Premium
  }
  |
  -- AdultIndividual premiums for ages 21, 27, 30, 40, 50 and 60
  AdultIndividual {
    adultIndividualAge21 :: Premium
    , adultIndividualAge27 :: Premium
    , adultIndividualAge30 :: Premium
    , adultIndividualAge40 :: Premium
    , adultIndividualAge50 :: Premium
    , adultIndividualAge60 :: Premium
  }
  |
  -- Couple premiums for ages 21, 30, 40, 50 and 60
  Couple {
    coupleAge21 :: Premium
    , coupleAge30 :: Premium
    , coupleAge40 :: Premium
    , coupleAge50 :: Premium
    , coupleAge60 :: Premium
  }
  |
  -- Couples with children premiums for ages 21, 30, 40 and 50
  CoupleWithChildren {
    coupleChildrenChildCount :: ChildCount
    , coupleChildrenAge21 :: Premium
    , coupleChildrenAge30 :: Premium
    , coupleChildrenAge40 :: Premium
    , coupleChildrenAge50 :: Premium
  }
  |
  -- Individuals with children premiums for ages 21, 30, 40 and 50
  IndividualWithChildren {
    individualChildrenChildCount :: ChildCount
    , individualChildrenAge21 :: Premium
    , individualChildrenAge30 :: Premium
    , individualChildrenAge40 :: Premium
    , individualChildrenAge50 :: Premium
  }
  deriving (Eq, Show)

-- some costs are stated in dollar amounts, whilst others are
-- textual descriptions (e.g. "20% Coinsurance after deductible")
data Cost =
  DollarCost {
    cost :: Double
  }
  |
  CostDescription {
    description :: String
  }
  deriving (Eq, Show)

data CostSharingPlan =
  CostSharingPlan {
    planName :: String
    , medicalDeductibleIndividual :: Cost
    , drugDeductibleIndividual :: Cost
    , medicalDeductibleFamily :: Cost
    , drugDeductibleFamily :: Cost
    , medicalMaxOOPIndividual :: Cost
    , drugMaxOOPIndividual :: Cost
    , medicalMaxOOPFamily :: Cost
    , drugMaxOOPFamily :: Cost
    , primaryCarePhysician :: Cost
    , specialist :: Cost
    , emergencyRoom :: Cost
    , inpatientFacility :: Cost
    , inpatientPhysician :: Cost
    , genericDrugs :: Cost
    , preferredBrandDrugs :: Cost
    , nonPreferredBrandDrugs :: Cost
    , specialityDrugs :: Cost
  }

data QHPLandscapeIndividualMarketMedical =
    QHPLandscapeIndividualMarketMedical {
      state :: State
      , county :: County
      , metalLevel :: MetalLevel
      , issuerName :: IssuerName
      , planIdStandardComponent :: PlanIDStandardComponent
      , planMarketingName :: PlanMarketingName
      , planType :: PlanType
      , ratingArea :: RatingArea
      , childOnlyOffering :: ChildOnlyOffering
      , source :: Source
      , customerServicePhoneNumbers :: CustomerServicePhoneNumbers
      , urls :: URLs
      , adultDental :: AdultDental
      , childDental :: ChildDental
      , childPremium :: PremiumScenario
      , adultIndividual :: PremiumScenario
      , couple :: PremiumScenario
      , coupleWithOneChild :: PremiumScenario
      , coupleWithTwoChildren :: PremiumScenario
      , coupleWithThreeOrMoreChildren :: PremiumScenario
      , individualWithOneChild :: PremiumScenario
      , individualWithTwoChildren :: PremiumScenario
      , individualWithThreeOrMoreChildren :: PremiumScenario
      , standardPlan :: CostSharingPlan
      , silver73PercentPlan :: CostSharingPlan
      , silver87PercentPlan :: CostSharingPlan
      , silver94PercentPlan :: CostSharingPlan
    }

-- take a String which may represent a Dollar amount or a textual
-- description and parse it into a Cost. this isn't smart enough
-- to determine that we have a malformed Dollar amount, but since
-- the fallback is a plaintext description, there's no way that
-- we could tell the difference short of NLP. the good news is
-- we don't have to wrap in an Either -- basically anything goes.
parseDollarCostOrDescription :: String -> Cost
parseDollarCostOrDescription xs = case parsedValue of
  Right p -> DollarCost p
  Left _ -> CostDescription xs
  where
    parsedValue = parseDollarAmount xs

parseChildPremiumScenario :: String -> Either ParseError PremiumScenario
parseChildPremiumScenario xs = Child <$> parseDollarAmount xs

parseAdultIndividualPremiumScenario :: String -> String -> String -> String -> String -> String ->
  Either ParseError PremiumScenario
parseAdultIndividualPremiumScenario s1 s2 s3 s4 s5 s6 =
  AdultIndividual <$> (parseDollarAmount s1) <*>
    (parseDollarAmount s2) <*> (parseDollarAmount s3) <*>
    (parseDollarAmount s4) <*> (parseDollarAmount s5) <*>
    (parseDollarAmount s6)

parseCouplePremiumScenario :: String -> String -> String -> String -> String ->
  Either ParseError PremiumScenario
parseCouplePremiumScenario s1 s2 s3 s4 s5 =
  Couple <$> (parseDollarAmount s1) <*>
    (parseDollarAmount s2) <*> (parseDollarAmount s3) <*>
    (parseDollarAmount s4) <*> (parseDollarAmount s5)

parseCoupleWithChildrenPremiumScenario :: String -> String -> String -> String -> String ->
  Either ParseError PremiumScenario
parseCoupleWithChildrenPremiumScenario s1 s2 s3 s4 s5 =
  CoupleWithChildren s1 <$>
    (parseDollarAmount s2) <*> (parseDollarAmount s3) <*>
    (parseDollarAmount s4) <*> (parseDollarAmount s5)

parseIndividualWithChildrenPremiumScenario :: String -> String -> String -> String -> String ->
  Either ParseError PremiumScenario
parseIndividualWithChildrenPremiumScenario s1 s2 s3 s4 s5 =
  IndividualWithChildren s1 <$>
    (parseDollarAmount s2) <*> (parseDollarAmount s3) <*>
    (parseDollarAmount s4) <*> (parseDollarAmount s5)

parseCostSharingPlan :: String -> String -> String -> String -> String ->
  String -> String -> String -> String -> String -> String -> String -> String ->
  String -> String -> String -> String -> String -> CostSharingPlan
parseCostSharingPlan s1 s2 s3 s4 s5 s6 s7 s8 s9 s10 s11
  s12 s13 s14 s15 s16 s17 s18 =
    CostSharingPlan s1 (parseDollarCostOrDescription s2)
      (parseDollarCostOrDescription s3) (parseDollarCostOrDescription s4)
      (parseDollarCostOrDescription s5) (parseDollarCostOrDescription s6)
      (parseDollarCostOrDescription s7) (parseDollarCostOrDescription s8)
      (parseDollarCostOrDescription s9) (parseDollarCostOrDescription s10)
      (parseDollarCostOrDescription s11) (parseDollarCostOrDescription s12)
      (parseDollarCostOrDescription s13) (parseDollarCostOrDescription s14)
      (parseDollarCostOrDescription s15) (parseDollarCostOrDescription s16)
      (parseDollarCostOrDescription s17) (parseDollarCostOrDescription s18)

parseQHPLandscapeIndividualMarketMedical :: String -> Either ParseError QHPLandscapeIndividualMarketMedical
parseQHPLandscapeIndividualMarketMedical [] = Left EmptyString
parseQHPLandscapeIndividualMarketMedical xs
  | fieldCount == 128 =
      QHPLandscapeIndividualMarketMedical
        (arr !! 0) -- State
        (arr !! 1) -- County
        (arr !! 2) -- MetalLevel
        (arr !! 3) -- IssuerName
        (arr !! 4) -- PlanIDStandardComponent
        (arr !! 5) -- PlanMarketingName
        (arr !! 6) -- PlanType
        (arr !! 7) -- RatingArea
        (arr !! 8) -- ChildOnlyOffering
        (arr !! 9) -- Source
        -- LocalNumber, TollFreeNumber and TTYNumber
        (CustomerServicePhoneNumbers (arr !! 10) (arr !! 11) (arr !! 12))
        -- NetworkURL, PlanBrochureURL, SummaryOfBenefitsURL, DrugFormularyURL
        (URLs (arr !! 13) (arr !! 14) (arr !! 15) (arr !! 16))
        (arr !! 17) -- AdultDental
        (arr !! 18) -- ChildDental
        <$> (parseChildPremiumScenario (arr !! 20)) <*>
        (parseAdultIndividualPremiumScenario (arr !! 21) (arr !! 22) (arr !! 23)
          (arr !! 24) (arr !! 25) (arr !! 26)) <*>
        (parseCouplePremiumScenario (arr !! 27) (arr !! 28) (arr !! 29)
          (arr !! 30) (arr !! 31)) <*>
        (parseCoupleWithChildrenPremiumScenario "1" (arr !! 32) (arr !! 33)
          (arr !! 34) (arr !! 35)) <*>
        (parseCoupleWithChildrenPremiumScenario "2" (arr !! 36) (arr !! 37)
          (arr !! 38) (arr !! 39)) <*>
        (parseCoupleWithChildrenPremiumScenario "3 or more" (arr !! 40)
          (arr !! 41) (arr !! 42) (arr !! 43)) <*>
        (parseIndividualWithChildrenPremiumScenario "1" (arr !! 44)
          (arr !! 45) (arr !! 46) (arr !! 47)) <*>
        (parseIndividualWithChildrenPremiumScenario "2" (arr !! 48)
          (arr !! 49) (arr !! 50) (arr !! 51)) <*>
        (parseIndividualWithChildrenPremiumScenario "3 or more" (arr !! 52)
          (arr !! 53) (arr !! 54) (arr !! 55)) <*>
        (Right $ parseCostSharingPlan "Standard Plan" (arr !! 57) (arr !! 58)
          (arr !! 59) (arr !! 60) (arr !! 61) (arr !! 62) (arr !! 63)
          (arr !! 64) (arr !! 65) (arr !! 66) (arr !! 67) (arr !! 68)
          (arr !! 69) (arr !! 70) (arr !! 71) (arr !! 72) (arr !! 73)) <*>
        (Right $ parseCostSharingPlan "73 Percent Actuarial Value Silver Plan"
          (arr !! 75) (arr !! 76) (arr !! 77) (arr !! 78) (arr !! 79)
          (arr !! 80) (arr !! 81) (arr !! 82) (arr !! 83) (arr !! 84)
          (arr !! 85) (arr !! 86) (arr !! 87) (arr !! 88) (arr !! 89)
          (arr !! 90) (arr !! 91)) <*>
        (Right $ parseCostSharingPlan "94 Percent Actuarial Value Silver Plan"
          (arr !! 93) (arr !! 94) (arr !! 95) (arr !! 96) (arr !! 97)
          (arr !! 98) (arr !! 99) (arr !! 100) (arr !! 101) (arr !! 102)
          (arr !! 103) (arr !! 104) (arr !! 105) (arr !! 106) (arr !! 107)
          (arr !! 108) (arr !! 109)) <*>
        (Right $ parseCostSharingPlan "94 Percent Actuarial Value Silver Plan"
          (arr !! 111) (arr !! 112) (arr !! 113) (arr !! 114) (arr !! 115)
          (arr !! 116) (arr !! 117) (arr !! 118) (arr !! 119) (arr !! 120)
          (arr !! 121) (arr !! 122) (arr !! 123) (arr !! 124) (arr !! 125)
          (arr !! 126) (arr !! 127))
    | otherwise = Left $ OtherParseError ("Expected 128 fields; found " ++ (show $ length arr))
  where
    arr = safeSplitCsv xs
    fieldCount = length arr
