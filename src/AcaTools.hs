module AcaTools where

import Data.Char

-- adapted from Real World Haskell; will switch to something else
-- later when I understand all this a little better.
(>>?) :: Either a b -> (b -> Either a d) -> Either a d
Left  y  >>? _ = Left y
Right z  >>? f = f z

-- parse a comma-delimited line which contains fields which themselves
-- contain commas. these lines are delimited by enclosing, in double
-- quotes, any field which itself contains a comma. this is a regrettable
-- situation, but unsurprising given legacy business applications which
-- preferentially delimit with commas even if you'd prefer something
-- a little easier to work with, e.g. a pipe symbol "|". this function
-- removes the enclosing double quotes in those cases where the field
-- was quoted to enclose commas. function assumes the only use of double
-- quotes is to enclose fields containing commas, but is in no way
-- dependent on that assumption.
-- example:
--   Field 1,Field 2,"$300,000.00",Field 4,"Field, I say"
-- bad input: a quoted field with any space between enclosing commas
--            and the quotes themselves
safeSplitCsv :: String -> [String]
safeSplitCsv [] = []
safeSplitCsv xs = go xs []
  where go remainder fields
         | remainder == [] = reverse fields
         | head remainder  == ',' = go (tail remainder) ("" : fields)
         | otherwise = go newRemainder newFields
           where
             currentField =
               if (head remainder == '"')
                 then takeWhile (\p -> p /= '"') (tail remainder)
                 else takeWhile (\p -> p /= ',') remainder
             offset = (length currentField) +
               if (head remainder == '"')
                 then 3
                 else 1
             newRemainder = drop offset remainder
             newFields = currentField : fields

data ParseError = EmptyString
                | IncorrectFieldCount Int Int
                | InvalidCurrencyCharacters String
                | OtherParseError String
                deriving (Eq, Show)

{- strip '$' and ',' from a currency String, front-padding if
   String starts with leading decimal (so "read" will succeed),
   ParseError if String contains any other non-digit characters.
   Also complains if dollar sign anywhere except first position,
   more than one decimal point, etc. -}
parseDollarAmount :: String -> Either ParseError Double
parseDollarAmount [] = Left EmptyString
parseDollarAmount xs
  | containsIllegalChars = Left $ InvalidCurrencyCharacters illegalChars
  | dollarSignPastFirstPosition = Left $ InvalidCurrencyCharacters "dollar sign(s) wrong position"
  | moreThanOneDecimalPoint = Left $ InvalidCurrencyCharacters ">1 decimal point"
  | otherwise = Right (read (padString cleanedString) :: Double)
  where isLegalChar c = isDigit c || c == '$' || c == '.' || c == ','
        containsIllegalChars = elem False $ map isLegalChar xs
        dollarSignPastFirstPosition = elem '$' $ tail xs
        moreThanOneDecimalPoint = (length $ filter (\p -> p == '.') xs) > 1
        illegalChars = filter (not . isLegalChar) xs
        cleanedString = filter (\p -> isDigit p || p == '.') xs
        padString s = if (head s) == '.' then '0':s else s
