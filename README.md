# ACA Dataset Parser

----
## What is the ACA Dataset Parser?
A Haskell library for parsing datasets associated with the Affordable Care Act, from the [Data.HealthCare.gov](https://data.healthcare.gov/) website. 

I currently have a parser for the "QHP Landscape Individual Market Medical" file; more coming later.

----

